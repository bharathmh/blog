# README #
This project is a simple blog application to write posts and comments for the posts.

Built on Ruby on rails framework, for database SqlLite is used.

Agile development methodology is used and app is built on several iterations. One iteration each week.
Uses the MVC architecture 

To, run this app on your system. 
Setup a Ruby on Rails environment using this document https://drive.google.com/file/d/0BxgsibnKgN7PYlViaE90LWxiQW8/view?usp=sharing

run "bundle install" command to install the gems

On your local machine: navigate to a folder where you want to clone the app.

1)Initialize a git using: "git init" command

2) clone the app: git clone https://bharathmh@bitbucket.org/bharathmh/blog.git

3) navigate to the app directory, then navigate to bin directory
use the command: rake db:migrate RAILS_ENV=development

4) then type: rails server
this will start the app on localhost on port 3000

5) goto a browser and type: http://localhost:3000/posts
this will open a console to write posts and comments for the posts
**** The username and password is admin/secret ****
 
You are good to go!